<!--

author:   Jakob Sablowski
edited:   
email:    mse-lab@et.tu-freiberg.de
version:  0.0.1
language: de

import:  https://github.com/LiaTemplates/Pyodide

Scripts:
@runFormula: <script>console.html(`<lia-formula formula="@'input" displayMode="true"></lia-formula>`);"LIA: stop"</script>

-->


# Messtechnik Praktikum SoSe 2024

Herzlich willkommen zum Praktikum der Lehrveranstaltung "Messtechnik" im Sommersemester 2024 an der TU Bergakademie Freiberg! 

## Einführung
Wärmepumpen spielen eine zentrale Rolle in der modernen Energie- und Gebäudetechnik. Sie werden genutzt um mittels elektrischer Energie zu heizen. Dabei sind sie deutlich effizienter als eine direkte elektrische Heizung. Im Rahmen des Praktikums geht es darum, die Effizienz einer Wärmepumpe messtechnisch zu bestimmen. 

In den 4 Praktikumsaufgaben werden jeweils Teilaspekte dieser komplexen Messaufgabe betrachtet. Die Lernziele des Praktikums sind: 

1. eine grundlegende Herangehensweise an komplexe Messaufgaben zu erarbeiten, und 
2. Verständnis und Übung einzelner Teilaspekte wie z.B.: Entwurf eines Messkonzepts, Temperaturmessung, Messunsicherheitsbetrachtung. 

Zu diesem Zweck sind die Praktikumsaufgaben offen gestaltet, das heißt: die Bearbeitung beinhaltet ausdrücklich das eigenständige Treffen von sinnvollen Annahmen und Vereinfachungen sowie eigenständige Recherche zum jeweiligen Thema. 

> *__Hinweis__: Die Abgabe der Aufgaben erfolgt als PDF-Datei. Knöpfe wie "Abschicken" in diesem Dokument haben keine Funktion. Nähere Informationen gibt es beim Einführungstermin zum Praktikum*


## Aufgabe 1 - Prozessverständnis und Messkonzept
Ziel des ersten Praktikums ist es, ein grundlegendes Prozessverständnis für den Untersuchungsgegenstand „Wärmepumpe“ zu erarbeiten. Auf dieser Grundlage soll ein Messkonzept entworfen werden, mit dem die Effizienz einer __Wasser-Wasser-Wärmepumpe__ charakterisiert werden kann. Hierfür sind die folgenden Teilaufgaben zu lösen.

<section>

### a) Funktionsprinzip 
Beschreiben Sie mit eigenen Worten das **grundlegende Funktionsprinzip** einer Wärmepumpe. Welche Aspekte sind für die vorliegende Messaufgabe relevant? (insgesamt mindestens 80 Wörter). 

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]

### b) Kennzahl
Welche Kennzahl beschreibt die Effizienz einer Wärmepumpe? Beschreiben Sie, was diese Kennzahl ausdrückt und geben Sie die dazugehörige Gleichung an! 

    [[___ ___ ___ ___]]

Gleichung[^1]:

```latex

```
@runFormula

[^1]: Formeln können im TeX-Format eingegeben werden, unterstütz werden folgende Funktionen: https://katex.org/docs/supported.html)

### c) Messgrößen und Schema
Die Kennzahl zur Effizienz der Wärmepumpe soll mit einem Versuchsstand experimentell bestimmt werden. Welche Messgrößen müssen hierfür an den Ein- und Austritten der Wärmepumpe erfasst werden? Nennen Sie alle benötigten Messgrößen und skizzieren Sie ein Fließschema, in dem die relevanten Messstellen dargestellt und benannt sind. 

> *__Hinweis__: Gehen Sie von einem möglichst einfachen Fall für eine Wasser-Wasser Wärmepumpe aus. Abstrahieren Sie das Schema soweit wie möglich. Die Wärmepumpe selbst kann als Kästchen („Black Box“) dargestellt werden.*

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]

??[excalidraw](https://excalidraw.com/)

### d) Messkonzept und Randbedingungen
Beschreiben Sie kurz das Messkonzept zur Bestimmung der Effizienz der Wärmepumpe. Welche Randbedingungen sollten bei der Messung berücksichtigt werden?

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]

</section>
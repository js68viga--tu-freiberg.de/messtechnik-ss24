<!--

author:   Jakob Sablowski
edited:   
email:    mse-lab@et.tu-freiberg.de
version:  0.0.1
language: de

import:  https://github.com/LiaTemplates/Pyodide

Scripts:
@runFormula: <script>console.html(`<lia-formula formula="@'input" displayMode="true"></lia-formula>`);"LIA: stop"</script>

-->

# Messtechnik Praktikum SoSe 2024

Herzlich willkommen zum Praktikum der Lehrveranstaltung "Messtechnik" im Sommersemester 2024 an der TU Bergakademie Freiberg! 


## Einführung
Wärmepumpen spielen eine zentrale Rolle in der modernen Energie- und Gebäudetechnik. Sie werden genutzt um mittels elektrischer Energie zu heizen. Dabei sind sie deutlich effizienter als eine direkte elektrische Heizung. Im Rahmen des Praktikums geht es darum, die Effizienz einer Wärmepumpe messtechnisch zu bestimmen. 

In den 4 Praktikumsaufgaben werden jeweils Teilaspekte dieser komplexen Messaufgabe betrachtet. Die Lernziele des Praktikums sind: 

1. eine grundlegende Herangehensweise an komplexe Messaufgaben zu erarbeiten, und 
2. Verständnis und Übung einzelner Teilaspekte wie z.B.: Entwurf eines Messkonzepts, Temperaturmessung, Messunsicherheitsbetrachtung. 

Zu diesem Zweck sind die Praktikumsaufgaben offen gestaltet, das heißt: die Bearbeitung beinhaltet ausdrücklich das eigenständige Treffen von sinnvollen Annahmen und Vereinfachungen sowie eigenständige Recherche zum jeweiligen Thema. 

> *__Hinweis__: Die Abgabe der Aufgaben erfolgt als PDF-Datei. Knöpfe wie "Abschicken" in diesem Dokument haben keine Funktion. Nähere Informationen gibt es beim Einführungstermin zum Praktikum*

## Aufgabe 2 - Temperatursensoren
Um die Effizient der Wasser-Wasser-Wärmepumpe zu bestimmen müssen mehrere Wassertemperaturen gemessen werden. Ziel des zweiten Praktikums ist die Auswahl und Bewertung von geeigneten Temperatursensoren für diese Messaufgabe. Hierfür sind die folgenden Teilaufgaben zu lösen.

###  a) Messprinzipien 
Nennen Sie 3 Arten von Sensoren für die elektrische Temperaturmessung. Beschreiben Sie kurz deren Messprinzip.

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]

### b) Auswahl Temperaturfühler
Für die Messung an der Wärmepumpe soll die Temperaturmessung die Anforderungen gemäß [DIN EN 14511-3](https://www.nautos.de/TBZ/search/item-detail/DE30093198) erfüllen. Diese Norm definiert Prüfverfahren für Wärmepumpen. Zur Messung einer Flüssigkeitstemperatur enthält die Norm folgende Angaben zur maximal zulässigen Messunsicherheit:

* Messgröße: Temperatur am Einlass/Auslass
* Einheit: °C
* maximal zulässige Messunsicherheit: ±0,15 K 
* Alle Unsicherheiten sind als maximale erweiterte Unsicherheiten in einem Vertrauensintervall von 95 % zu verstehen.

> *__Hinweis__: Die oben angegebene maximal zulässige Messunsicherheit von ±0,15 K entspricht einer maximalen Grenzabweichung von 0,13 K. Der Umgang mit Messunsicherheiten ist Thema einer späteren Aufgabe.*

Wählen Sie auf Grundlage dieser Anforderungen einen geeigneten Temperatursensor aus! Geben Sie hierfür einen konkreten Temperatursensor (Herstellerbezeichnung, Modellnummer oder Ähnliches) an, der bei einem Anbieter Ihrer Wahl verfügbar ist. Geben Sie die relevanten technischen Daten des gewählten Sensors an (z.B. durch Angabe einer URL zum gewählten Modell, Datenblatt oder Katalogausschnitt). Begründen Sie Ihre Auswahl.

> *__Hinweis__: Bei der Auswahl soll es nur um den Temperaturfühler gehen. Anschluss des Sensors, Messdatenerfassung, etc. müssen an dieser Stelle nicht berücksichtigt werden.*

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]


### c) Kennlinienbestimmung
Für die Messung der Umgebungstemperatur soll ein vorhandener Temperatursensor (Messwiderstand) verwendet werden. Der Temperatursensor soll zunächst kalibriert werden. Hierfür wurde der Widerstand des Sensors bei verschiedenen Temperaturen in einem Kalibierofen gemessen. Die Ergebnisse der Messung lauten: 

| **Temperatur in °C** | **Widerstand in Ohm** |
| -------------------- | --------------------- |
| 10.0                 | 103.96                |
| 20.0                 | 107.82                |
| 30.0                 | 111.64                |
| 40.0                 | 115.60                |
| 50.0                 | 119.40                |
| 60.0                 | 123.20                |
| 70.0                 | 126.97                |
| 100.0                | 138.52                |
| 200.0                | 175.99                |
| 300.0                | 212.07                |
| 400.0                | 247.37                |
| 800.0                | 375.97                |

> *__Hinweis__: Die Messunsicherheiten der angegebenen Werte werden aus didaktischen Gründen an dieser Stelle vernachlässigt. Der Umgang mit Messunsicherheiten ist Thema einer späteren Aufgabe.*

Ermitteln Sie anhand der gegebenen Werte eine geeignete Kennlinie für die Temperaturmessung. Stellen Sie die Kennlinie und die Messwerte grafisch dar. Geben Sie die Gleichung für die ermittelte Kennlinie an.


Für die Kennlinienbestimmung und die Darstellung _können_ Sie den folgenden Python-Code nutzen und erweitern (Beispiele siehe: [`np.polyfit`](https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html))

```python
import numpy as np
import matplotlib.pyplot as plt

# Messwerte
temperatur = [ 10,  20,  30,  40,  50,  60,  70, 100, 200, 300, 400, 800]
widerstand = [103.85, 107.81, 111.72, 115.58, 119.36, 123.24, 126.99, 138.61, 175.68, 212.09, 247.35, 376.15]

# Kennlinienbestimmung


# grafische Darstellung
plt.clf()
plt.plot(temperatur, widerstand, 'x', label='Messwerte')
plt.xlabel('Temperatur in °C')
plt.ylabel('Widerstand in Ohm')
plt.legend()
plt.show()
```
@Pyodide.eval


Gleichung der Kennlinie[^1]:

```latex

```
@runFormula

[^1]: Formeln können im TeX-Format eingegeben werden, unterstütz werden folgende Funktionen: https://katex.org/docs/supported.html)

### d) Einbauposition
Erörtern Sie kurz den Einfluss der Einbauposition auf die Temperaturmessung in den Wasserrohren an den Ein- und Austritten der Wärmepumpe!

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]
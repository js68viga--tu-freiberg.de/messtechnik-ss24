<!--

author:   Jakob Sablowski
edited:   
email:    mse-lab@et.tu-freiberg.de
version:  0.0.1
language: de

import:  https://github.com/LiaTemplates/Pyodide

Scripts:
@runFormula: <script>console.html(`<lia-formula formula="@'input" displayMode="true"></lia-formula>`);"LIA: stop"</script>

-->

# Messtechnik Praktikum SoSe 2024

Herzlich willkommen zum Praktikum der Lehrveranstaltung "Messtechnik" im Sommersemester 2024 an der TU Bergakademie Freiberg! 


## Einführung
Wärmepumpen spielen eine zentrale Rolle in der modernen Energie- und Gebäudetechnik. Sie werden genutzt um mittels elektrischer Energie zu heizen. Dabei sind sie deutlich effizienter als eine direkte elektrische Heizung. Im Rahmen des Praktikums geht es darum, die Effizienz einer Wärmepumpe messtechnisch zu bestimmen. 

In den 4 Praktikumsaufgaben werden jeweils Teilaspekte dieser komplexen Messaufgabe betrachtet. Die Lernziele des Praktikums sind: 

1. eine grundlegende Herangehensweise an komplexe Messaufgaben zu erarbeiten, und 
2. Verständnis und Übung einzelner Teilaspekte wie z.B.: Entwurf eines Messkonzepts, Temperaturmessung, Messunsicherheitsbetrachtung. 

Zu diesem Zweck sind die Praktikumsaufgaben offen gestaltet, das heißt: die Bearbeitung beinhaltet ausdrücklich das eigenständige Treffen von sinnvollen Annahmen und Vereinfachungen sowie eigenständige Recherche zum jeweiligen Thema. 

> *__Hinweis__: Die Abgabe der Aufgaben erfolgt als PDF-Datei. Knöpfe wie "Abschicken" in diesem Dokument haben keine Funktion. Nähere Informationen gibt es beim Einführungstermin zum Praktikum*

## Aufgabe 3 - Widerstandsmessung
Um die Effizienz der Wasser-Wasser-Wärmepumpe zu bestimmen müssen mehrere Wassertemperaturen gemessen werden. Hierfür sollen PT100 Widerstandstemperatursensoren verwendet werden. Im dritten Praktikum geht es um den elektrischen Anschluss dieser Sensoren. Hierfür sind die folgenden Teilaufgaben zu lösen.

###  a) Wheatstone-Brücke 
Der elektrische Widerstand eines PT100 Widerstandssensors soll über eine Wheatstonesche Messbrücke mit der Ausschlagsmethode gemessen werden. Skizzieren Sie die Schaltung und beschriften Sie alle wesentlichen Größen. Dimensionieren Sie die Festwiderstände der Messbrücke so, dass die Brücke bei einer Temperatur von 0 °C am PT100 abgeglichen ist. 

??[excalidraw](https://excalidraw.com/)

Widerstandswerte[^1]:

```latex
R_2 =           \\
R_3 =           \\  
R_4 =           \\ 

```
@runFormula

[^1]: Formeln können im TeX-Format eingegeben werden, unterstütz werden folgende Funktionen: https://katex.org/docs/supported.html)

### b) Vergleich direkte Widerstandsmessung
Worin besteht der Vorteil gegenüber einer direkten Widerstandsmessung?

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]


### c) Diagonalspannung
Geben Sie die Gleichungen $ U_\mathrm{d}(R) $ und $ U_\mathrm{d}(\vartheta) $ für die Brücken-Diagonalspannung $ U_\mathrm{d} $ an. Verwenden Sie hierfür die linearisierte Kennlinie des PT100. 
Wählen Sie eine Brücken-Versorgungsspannung $ U_0 $ und einen sinnvollen Wertebereich für die zu messenden Temperaturen. Stellen Sie den Zusammenhang $ U_\mathrm{d}(\vartheta) $ grafisch dar. 

Gleichungen[^1]:

```latex



```
@runFormula

Darstellung:

```python
import numpy as np
import matplotlib.pyplot as plt

plt.clf()
plt.xlabel("Temperatur in K")
plt.ylabel("Brücken-Diagonalspannung in V")
plt.grid()
plt.show()
```
@Pyodide.eval


[^1]: Formeln können im TeX-Format eingegeben werden, unterstütz werden folgende Funktionen: https://katex.org/docs/supported.html)

### d) Quantisierung
Die Brückendiagonalspannung $ U_\mathrm{d} $ soll nun mit einem Analog-Digital-Umsetzer (ADC, analog-to-digital converter) in ein digitales Signal umgesetzt werden. Legen Sie hierfür eine geeignete Quantisierungsstufe $ q $ (in mV) fest. Die maximale Messabweichung aufgrund der Quantisierung soll weniger als 0,1 K betragen. Diskutieren Sie die Zusammenhänge zwischen der Brücken-Versorgungsspannung, dem Eingangsspannungsbereich des ADC und der Anzahl der Quantisierungsschritte.

Gleichungen[^1]:

```latex



```
@runFormula

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]


[^1]: Formeln können im TeX-Format eingegeben werden, unterstütz werden folgende Funktionen: https://katex.org/docs/supported.html)


<!--

author:   Jakob Sablowski
edited:   
email:    mse-lab@et.tu-freiberg.de
version:  0.0.1
language: de

import:  https://github.com/LiaTemplates/Pyodide

Scripts:
@runFormula: <script>console.html(`<lia-formula formula="@'input" displayMode="true"></lia-formula>`);"LIA: stop"</script>

-->

# Messtechnik Praktikum SoSe 2024

Herzlich willkommen zum Praktikum der Lehrveranstaltung "Messtechnik" im Sommersemester 2024 an der TU Bergakademie Freiberg! 


## Einführung
Wärmepumpen spielen eine zentrale Rolle in der modernen Energie- und Gebäudetechnik. Sie werden genutzt um mittels elektrischer Energie zu heizen. Dabei sind sie deutlich effizienter als eine direkte elektrische Heizung. Im Rahmen des Praktikums geht es darum, die Effizienz einer Wärmepumpe messtechnisch zu bestimmen. 

In den 4 Praktikumsaufgaben werden jeweils Teilaspekte dieser komplexen Messaufgabe betrachtet. Die Lernziele des Praktikums sind: 

1. eine grundlegende Herangehensweise an komplexe Messaufgaben zu erarbeiten, und 
2. Verständnis und Übung einzelner Teilaspekte wie z.B.: Entwurf eines Messkonzepts, Temperaturmessung, Messunsicherheitsbetrachtung. 

Zu diesem Zweck sind die Praktikumsaufgaben offen gestaltet, das heißt: die Bearbeitung beinhaltet ausdrücklich das eigenständige Treffen von sinnvollen Annahmen und Vereinfachungen sowie eigenständige Recherche zum jeweiligen Thema. 

> *__Hinweis__: Die Abgabe der Aufgaben erfolgt als PDF-Datei. Knöpfe wie "Abschicken" in diesem Dokument haben keine Funktion. Nähere Informationen gibt es beim Einführungstermin zum Praktikum*

## Aufgabe 4 - Bestimmung der Leistungszahl
Die Leistungszahl (COP) der Wärmepumpe wird nun an einem Teststand gemessen werden. Die  Messungen werden jeweils im Beharrungszustand durchgeführt. 
Hierfür werden im Heizbetrieb der Wasser-Wasser Wärmepumpe zunächst folgende Prüfbedingungen an dem Teststand eingestellt:

* Austrittstemperatur Innenwärmeübertrager (kondensatorseitig): 35 °C 
* Eintrittstemperatur Innenwärmeübertrager (kondensatorseitig): 30 °C
* Eintrittstemperatur Außenwärmeübertrager (verdampferseitig): 10 °C


###  a) Ishikawa-Diagramm
Stellen Sie die Einflüsse auf das Messergebnis (Leistungszahl) in Form eines Ishikawa Diagramms (Ursache-Wirkungs-Diagramm) dar.

??[excalidraw](https://excalidraw.com/)


### b) Messwerte
Zunächst wird der Wasserkreislauf am Innenwärmeübertrager (kondensatorseitig) betrachtet. Folgende Werte wurden für die Temperaturen $ \vartheta $ und den Volumenstrom $ \dot{V} $ des Wassers erfasst:

$ \vartheta_{\mathrm{aus}} = \{35{,}088; 35{,}020; 35{,}048; 35{,}112; 35{,}093; 34{,}951; 35{,}047; 34{,}992; 34{,}994; 35{,}020\} \rm{°C}$

$ \vartheta_{\mathrm{ein}} = \{30{,}010; 30{,}101; 30{,}053; 30{,}008; 30{,}031; 30{,}023; 30{,}104; 29{,}985; 30{,}021; 29{,}940\} \rm{°C} $

$ \dot{V} = \{0{,}7948; 0{,}8013; 0{,}8017; 0{,}7985; 0{,}8045\} \, \rm{l/s} $

Darüber hinaus sind folgende Angaben zu den verwendeten Messgeräten bekannt:

* Toleranz der Temperaturfühler: 0,1 K
* Toleranz des Volumenstrommessgeräts: 0,02 l/s

Geben Sie für auf Grundlage dieser Angaben den Messwert und die Standardunsicherheit für die beiden Temperaturen und den Volumenstrom nach GUM an!

Berechnungen:

```python
import numpy as np

# Messwerte
temperatur_austritt = [35.088, 35.020, 35.048, 35.112, 35.093, 34.951, 35.047, 34.992, 34.994, 35.020]
temperatur_eintritt = [30.010, 30.101, 30.053, 30.008, 30.031, 30.023, 30.104, 29.985, 30.021, 29.940]
volumenstrom = [0.7948, 0.8013, 0.8017, 0.7985, 0.8045]



```
@Pyodide.eval

Ergebnisse:

    [[___ ___ ___ ___ ___ ___ ___]]


### c) Messergebnis
Für die elektrische Leistungsmessung der Wärmepumpe liegt folgendes Messergebnis vor:

Messwert: $ P_{\rm{el}} = 4.1 \, \rm{kW} $, Standardunsicherheit: $ u_{P_{\rm{el}}} = 0.02 \, \rm{kW} $.

Geben Sie das vollständige Messergebnis für die Leistungszahl (COP) mit einem Erweiterungsfaktor von k = 2 an!



Berechnungen (Python):

```python
 

```
@Pyodide.eval

Berechnungen / Ergebnis (LaTex)[^1]:

```latex










```
@runFormula


[^1]: Formeln können im TeX-Format eingegeben werden, unterstütz werden folgende Funktionen: https://katex.org/docs/supported.html)

### d) Weitere Einflüsse
Nennen sie weitere mögliche Einflüsse auf das Messergebnis, die bei der Abschätzung der Messunsicherheit nicht berücksichtigt wurden. 

    [[___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___]]


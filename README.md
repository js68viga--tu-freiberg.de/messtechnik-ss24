# Messtechnik SS24
[![CC BY 4.0][cc-by-shield]][cc-by]

Unterlagen zum Praktikum der Lehrveranstaltung "Messtechnik" an der TU Freiberg im Sommersemester 2024. 

Informationen zur Lehrveranstaltung finden Sie im [OPAL](https://bildungsportal.sachsen.de/opal/auth/RepositoryEntry/34701279242?3).

## Praktikumsaufgaben

[Praktikum 1](https://liascript.github.io/course/?https://api.allorigins.win/raw?url=https://gitlab.hrz.tu-chemnitz.de/js68viga--tu-freiberg.de/messtechnik-ss24/-/raw/main/P-1/mt-praktikum-1.md#1)

[Praktikum 2](https://liascript.github.io/course/?https://api.allorigins.win/raw?url=https://gitlab.hrz.tu-chemnitz.de/js68viga--tu-freiberg.de/messtechnik-ss24/-/raw/main/P-2/mt-praktikum-2.md#1)

[Praktikum 3](https://liascript.github.io/course/?https://api.allorigins.win/raw?url=https://gitlab.hrz.tu-chemnitz.de/js68viga--tu-freiberg.de/messtechnik-ss24/-/raw/main/P-3/mt-praktikum-3.md#1)

[Praktikum 4](https://liascript.github.io/course/?https://api.allorigins.win/raw?url=https://gitlab.hrz.tu-chemnitz.de/js68viga--tu-freiberg.de/messtechnik-ss24/-/raw/main/P-4/mt-praktikum-4.md#1)


## Lizenz

Diese Arbeit unterliegt den Bestimmungen einer
[Creative Commons Namensnennung 4.0 International-Lizenz][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: https://creativecommons.org/licenses/by/4.0/deed.de
[cc-by-image]: https://licensebuttons.net/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
